FROM besbaevmedet/tamos
RUN sed -i 's/TLSv1,//g' /usr/local/openjdk-11/conf/security/java.security
RUN sed -i 's/  TLSv1.1, //g' /usr/local/openjdk-11/conf/security/java.security


RUN sed -i 's/MinProtocol = TLSv1.2/MinProtocol = TLSv1.0/' /etc/ssl/openssl.cnf
RUN sed -i 's/CipherString = DEFAULT@SECLEVEL=2/CipherString = DEFAULT@SECLEVEL=1/' /etc/ssl/openssl.cnf


ENTRYPOINT ["java","-jar","/tamos.jar"]
